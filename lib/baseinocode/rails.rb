# -*- encoding : utf-8 -*-
require "baseinocode/rails/version"
require "baseinocode/rails/string"
require "baseinocode/rails/base"

module Baseinocode
  module Rails
    class Engine < ::Rails::Engine
    end
  end
end
