$(document).on("page:change", function() {
  $("[data-range]").each(function() {
    var input = $(this).parent().find("input");
    var split = input.val().split(" - ");
    var de = undefined;
    var ate = undefined;
    if (split.length == 2) {
      de = split[0];
      ate = split[1];
    }

    input.daterangepicker({
      format: "DD/MM/YYYY",
      startDate: de,
      endDate: ate
    });

    $(this).on("click", function(e) {
      e.preventDefault();
      $(this).parent().find("input").trigger("click");
    });


    $(this).parents(".form-group, .row").first().find("small a").on("click", function(e) {
      var input = $(this).parent().next().find("input");
      input.val("").trigger("input");
      e.preventDefault();
    });
  });
});