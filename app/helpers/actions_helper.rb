# -*- encoding : utf-8 -*-
module ActionsHelper
  VISIBLE_ACTIONS = {
    index: [:create],
    new: [:save, :cancel],
    create: [:save, :cancel],
    edit: [:save, :cancel, :create],
    update: [:save, :cancel, :create],
    show: [:create, :back, :edit, :destroy]
  }

  def title(model)
    if action_name == "index"
      t("models.#{model.to_s.pluralize}")
    else
      "#{t("models.#{model}")} <i class='icon-angle-right'></i> #{t("actions.#{action_name}")}"
    end
  end

  def show_action(model, action)
    VISIBLE_ACTIONS[action_name.to_sym].include?(action)
  end

  def actions(model, cancan = true, &block)
    render partial: "general/actions", locals: {model: model, cancan: cancan, block: block}
  end

  def show_input_tag(model, field, value = nil)
    render partial: "general/show_input_tag", locals: {model: model, field: field, value: value || model.send(field)}
  end

  def btn_clear_filters
    link_to "Limpar Pesquisa", {action: :index, controller: controller_name, clear: true}, class: "btn btn-default pull-right btn-sm"
  end
end
